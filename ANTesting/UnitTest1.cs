﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ANT;
using System.Numerics;


namespace ANTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        [TestMethod]
        public void TestANTAdd()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            
            AlgebraicNumber q = new AlgebraicNumber(qi, f);

            Rational[] rqi = new Rational[2] { 4, 2 };
            AlgebraicNumber val = new AlgebraicNumber(rqi, f);

            Assert.AreEqual(val, q + q);
        }
        [TestMethod]
        public void TestANTSub()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            AlgebraicNumber q = new AlgebraicNumber(qi, f);

            Rational[] rqi = new Rational[2] { 0, 0 };
            AlgebraicNumber val = new AlgebraicNumber(rqi,f);

            Assert.AreEqual(val, q - q);
        }
        [TestMethod]
        public void TestANTMul()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            AlgebraicNumber q = new AlgebraicNumber(qi, f);

            //Polynomial poly = new Polynomial(qi);
            //var modulo = new Polynomial(minp);
            //Console.WriteLine((poly * poly) % modulo);

            Rational[] rqi = new Rational[2] { 3, 3 };
            AlgebraicNumber val = new AlgebraicNumber(rqi, f);

            Assert.AreEqual(val, q * q);
        }
        [TestMethod]
        public void TestUtilsResultant()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Rational[] q = new Rational[3] { 2, 0, 3 };
            Assert.AreEqual(707, ANT.Utils.Resultant(p, q));
            p = new Rational[4] { 1, 2, 0, 1 };
            q = new Rational[3] { 2, 0, 3 };
            Assert.AreEqual(59, ANT.Utils.Resultant(p, q));
        }
        [TestMethod]
        public void TestUtilsDiscriminant()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Assert.AreEqual(-707, ANT.Utils.PolynomialDiscriminant(p));
        }
        [TestMethod]
        public void TestUtilsPolynomialContent()
        {
            Rational[] p = new Rational[2] { new Rational(13,6),new Rational(3,4) };
            Assert.AreEqual(new Rational(1,12), ANT.Utils.PolynomialContent(p));
        }
        [TestMethod]
        public void TestPolyString()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Console.WriteLine(poly);
        }
        public void TestPolyAdd()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Rational[] r = new Rational[4] { 10, 4, 0, 2 };
            Polynomial result = new Polynomial(r);
            Assert.AreEqual(result, poly+poly);
        }
        [TestMethod]
        public void TestPolyMul()
        {

            Rational[] polyc = new Rational[2] { 1, 1 };
            Rational[] polyc2 = new Rational[3] { 1, 1, 1 };
            Polynomial poly = new Polynomial(polyc);
            Polynomial poly2 = new Polynomial(polyc2);
            Console.WriteLine(poly);
            Console.WriteLine(poly2);
            var prod = poly * poly2;

            Rational[] resc = new Rational[4] { 1,2,2,1};
            Polynomial result = new Polynomial(resc);

            Assert.AreEqual(result, prod);
        }
        [TestMethod]
        public void TestPolyLeadingTerm()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Assert.AreEqual((Rational)1, poly.LeadingTerm());
        }
        [TestMethod]
        public void TestPolyCont()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Assert.AreEqual((Rational)1, poly.Cont());
        }
        [TestMethod]
        public void TestPolyDiff()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Rational[] d = new Rational[3] {2, 0, 3 };
            Polynomial poly = new Polynomial(p);
            Polynomial pdiff = new Polynomial(d);
            Assert.AreEqual(pdiff, poly.Diff());
        }
        [TestMethod]
        public void TestPolyDiscriminant()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Assert.AreEqual(-707, poly.Discriminant());
        }
        [TestMethod]
        public void TestPolyEquals()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Assert.AreNotEqual(null, poly);
            Assert.IsFalse(poly.Equals(null));
        }
        [TestMethod]
        public void TestPolyEuclideanDivision()
        {
            Rational[] p1 = new Rational[] { 5, 1 };
            Rational[] p2 = new Rational[] { 3, 1 };
            Polynomial poly1 = new Polynomial(p1);
            Polynomial poly2 = new Polynomial(p2);
            var poly = poly1 * poly2;
            Assert.AreEqual<Polynomial>(poly1, Polynomial.EuclideanDivision(poly, poly2)[0]);
            Assert.AreEqual<Polynomial>( Polynomial.EuclideanDivision(poly, poly2)[0],poly/poly2);
            Assert.AreEqual<Polynomial>(Polynomial.EuclideanDivision(poly, poly2)[1], poly % poly2);
            Rational[] p3 = new Rational[] { new Rational(1,1), new Rational(1, 5) };
            var poly3 = new Polynomial(p3);
            Assert.AreEqual<Polynomial>(poly3, poly1 / (new Rational(5, 1)));
        }
        [TestMethod]
        public void TestPolyGCD()
        {
            Rational[] p1 = new Rational[] { 5, 1 };
            Rational[] p2 = new Rational[] { 3, 1 };
            Polynomial poly1 = new Polynomial(p1);
            Polynomial poly2 = new Polynomial(p2);
            var poly = poly1 * poly2;
            Assert.AreEqual<Polynomial>(poly1, Polynomial.GCD(poly, poly1));
            p1 = new Rational[] { 5 };
            poly1 = new Polynomial(p1);
            poly = poly1 * poly2;
            Assert.AreEqual<Polynomial>(poly, Polynomial.GCD(poly, poly1));
        }
        [TestMethod]
        public void TestPolyConstantTerm()
        {
            Rational[] p = new Rational[4] { 5, 2, 0, 1 };
            Polynomial poly = new Polynomial(p);
            Assert.AreEqual((Rational)5, poly.ConstantTerm());
            poly = new Polynomial(new Rational[1] { 0 });
            Assert.AreEqual<Rational>(0, poly.ConstantTerm());
            Assert.AreEqual<Rational>(0, poly[0]);
            poly[0] = 1;
            Assert.AreEqual<Rational>(1, poly[0]);
        }
        [TestMethod]
        public void AdditionTest()
        {
            Polynomial sqrt2 = new Polynomial(new Rational[] { -2,0, 1 });
            Polynomial sqrt3 = new Polynomial(new Rational[] { -3,0, 1 });
            //Console.WriteLine(sqrt2);
            //Console.WriteLine(sqrt3);

            var A = sqrt2.CompanionMatrix();
            var I = Matrix.Identity(A.Rows());
            var B = sqrt3.CompanionMatrix();
            Matrix sum = Matrix.KroneckerProduct(A, I) + Matrix.KroneckerProduct(I, B);
            Console.WriteLine(new Polynomial(sum.CharacteristicPolynomialCoefficients()));
        }

        [TestMethod]
        public void TestAntEquals()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            NumberField f2 = new NumberField(new Polynomial(minp), theta*theta);
            AlgebraicNumber q = new AlgebraicNumber(qi,f);
            Rational[] badqi = new Rational[2] { 2, 23 };
            Assert.IsFalse(q.Equals(null));

            AlgebraicNumber qbad = new AlgebraicNumber(badqi, f);
            Assert.IsFalse(q.Equals(qbad));

            qbad = new AlgebraicNumber(qi, f2);
            Assert.IsFalse(q.Equals(qbad));
        }
        [TestMethod]
        public void TestAntNorm()
        {
            Rational[] qi = new Rational[2] { 0, 1 };
            Rational[] minp = new Rational[3] { -2, 0, 1 };
            Complex theta = new Complex(1.41, 0);//Sqrt[2]
            NumberField f = new NumberField(new Polynomial(minp), theta);
            AlgebraicNumber q = new AlgebraicNumber(qi, f);
            Assert.AreEqual<Rational>(-2, q.Norm());
        }
        [TestMethod]
        public void TestNumberFieldReducePoly()
        {
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            Console.WriteLine(f.ReductionPolynomial);
        }
        [TestMethod]
        public void TestAntGetHashCode()
        {
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            var q = f.CreateFieldElement(new Rational[] { 1,2});
            Assert.IsNotNull(q.GetHashCode());
        }
        [TestMethod]
        public void TestAntToString()
        {
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            var q = f.CreateFieldElement(new Rational[] { 1, 2 });
            Assert.AreEqual<String>("1/1T^0 + 2/1T^1", q.ToString());
        }
        [TestMethod]
        public void TestAntEval()
        {
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            var q = f.CreateFieldElement(new Rational[] { 1, 2 });
            var expected = new Complex(2.22044604925031E-16, 1.73205);
            Assert.IsTrue((q.eval() - expected).Magnitude < .0000000001);
        }
        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestAntAddThrows()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            NumberField f2 = new NumberField(new Polynomial(minp), theta*theta);

            var q = f.CreateFieldElement(qi);
            var badq = f2.CreateFieldElement(qi);
            var throws = q+badq;
        }
        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestAntSubThrows()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            NumberField f2 = new NumberField(new Polynomial(minp), theta * theta);

            var q = f.CreateFieldElement(qi);
            var badq = f2.CreateFieldElement(qi);
            var throws = q - badq;
        }
        [TestMethod]
        [ExpectedException(typeof(ArithmeticException))]
        public void TestAntMulThrows()
        {
            Rational[] qi = new Rational[2] { 2, 1 };
            Rational[] minp = new Rational[3] { 1, 1, 1 };
            Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
            NumberField f = new NumberField(new Polynomial(minp), theta);
            NumberField f2 = new NumberField(new Polynomial(minp), theta * theta);

            var q = f.CreateFieldElement(qi);
            var badq = f2.CreateFieldElement(qi);
            var throws = q * badq;
        }
        [TestMethod]
        public void TestRationalCeiling()
        {
            Rational q = new Rational(1, 2);
            Assert.AreEqual<BigInteger>(1, q.Ceiling());
        }
        [TestMethod]
        public void TestRationalFloor()
        {
            Rational q = new Rational(1, 1);
            Assert.AreEqual<BigInteger>(1, q.Floor());
            q = new Rational(-1, 1);
            Assert.AreEqual<BigInteger>(-1, q.Floor());
            q = new Rational(23, 2);
            Assert.AreEqual<BigInteger>(11, q.Floor());
            q = new Rational(-1, 23);
            Assert.AreEqual<BigInteger>(0, q.Floor());
        }
        [TestMethod]
        public void TestRationalEquals()
        {
            Rational q = new Rational(1, 1);
            Assert.IsFalse(q.Equals(null));
            Assert.IsTrue((Rational)null == (Rational)null);
            Assert.IsFalse(q != q);
        }
        [TestMethod]
        public void TestRationalRandom()
        {
            Rational q = Rational.Random();
            Assert.IsFalse(q != null);
        }
        [TestMethod]
        public void TestRationalConstructor()
        {
            Rational q = new Rational();
            Assert.IsFalse(q!= null);
        }
    }
}
