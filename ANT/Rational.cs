﻿using System;
using System.Numerics;

namespace ANT
{
    public class Rational
    {
        BigInteger numerator;
        BigInteger denominator;
        public Rational()
        {
            numerator = 1;
            denominator = 1;
        }
        public Rational(BigInteger n, BigInteger d)
        {
            numerator = n;
            denominator = d;
            this.Reduce();
        }
        public Rational(BigInteger n)
        {
            numerator = n;
            denominator = 1;
        }
        public Rational Abs()
        {
            return new Rational(BigInteger.Abs(this.numerator), BigInteger.Abs(this.denominator));
        }
        //  User-defined conversion from double to Digit 
        public static implicit operator Rational(int d)
        {
            return new Rational((BigInteger)d);
        }
        //  User-defined conversion from double to Digit 
        public static implicit operator Complex(Rational d)
        {
            return new Complex((double)d.numerator / (double)d.denominator, 0);
        }

        //  User-defined conversion from double to Digit 
        public static implicit operator double(Rational d)
        {
            return (double)d.numerator / (double)d.denominator;
        }


        public static Rational Random()
        {
            var _rand = new Random();
            var numerator = (BigInteger)_rand.Next();
            return new Rational(numerator);
        }

        public static Rational One()
        {
            return new Rational(1, 1);
        }
        public static Rational Zero()
        {
            return new Rational(0, 1);
        }


        public Rational Pow(int p)
        {
            var nv = new Rational(BigInteger.Pow(numerator, p), BigInteger.Pow(denominator, p));
            nv.Reduce();
            return nv;
        }

        public static Rational GCD(Rational a, Rational b)
        {
            var num = BigInteger.GreatestCommonDivisor(a.numerator * b.denominator, b.numerator * a.denominator);
            var dem = a.denominator * b.denominator;
            return new Rational(num, dem);
        }

        private void Reduce()
        {
            var gcd = BigInteger.GreatestCommonDivisor(numerator, denominator);
            numerator /= gcd;
            denominator /= gcd;
            if (numerator == 0)
                denominator = 1;
        }

        /// <summary>
        /// Returns the first integer n  such that x less than or equal to n
        /// </summary>
        public BigInteger Floor()
        {
            if (denominator == 1)
                return numerator;
            var count = 0;
            var tmp = numerator;
            var sgn = 1;
            if (tmp < 0)
            {
                tmp *= -1;
                sgn = -1;
            }
            while (tmp >= denominator)
            {
                count++;
                tmp = tmp - denominator;

            }
            return sgn * count;
        }

        /// <summary>
        /// Returns the first integer n such that this less than n
        /// </summary>
        public BigInteger Ceiling()
        {
            return Floor() + 1;
        }
        public Rational Reciprocal()
        {
            return new Rational(denominator, numerator);
        }
        public static Rational operator /(Rational a, Rational b)
        {
            return a * b.Reciprocal();
        }
        public static Rational operator *(Rational a, Rational b)
        {
            var nv = new Rational(a.numerator * b.numerator, a.denominator * b.denominator);
            nv.Reduce();
            return nv;
        }
        public static Rational operator *(int a, Rational b)
        {
            var nv = new Rational(a * b.numerator, b.denominator);
            nv.Reduce();
            return nv;
        }
        public static Rational operator +(Rational a, Rational b)
        {
            var nv = new Rational(a.numerator * b.denominator + b.numerator * a.denominator, a.denominator * b.denominator);
            nv.Reduce();
            return nv;
        }
        public static Rational operator -(Rational a, Rational b)
        {
            var nv = new Rational(a.numerator * b.denominator - b.numerator * a.denominator, a.denominator * b.denominator);
            nv.Reduce();
            return nv;
        }
        public static bool operator !=(Rational a, Rational b)
        {
            if ((object)a == null || (object)b == null)
                return false;
            return a != null && a.Equals(b);
        }
        public static bool operator ==(Rational a, Rational b)
        {
            if ((object)a == null && (object)b == null)
                return true;
            return a != null && a.Equals(b);
        }
        public override string ToString()
        {
            return numerator.ToString() + "/" + denominator.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj is Rational)
            {
                var tmp = obj as Rational;
                if (tmp.numerator.CompareTo(numerator) == 0 && denominator.CompareTo(tmp.denominator) == 0)
                    return true;
                return false;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return numerator.GetHashCode() + denominator.GetHashCode();
        }
    }
}

