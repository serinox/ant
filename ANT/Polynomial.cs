﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
namespace ANT
{
    public class Polynomial
    {
        Rational[] coefficients; //lowest to highest degree
        int degree;//this is the degree of the polynomial

        /// <summary>
        /// Creates a polynomial from the provided coefficients. 
        /// Will remove all zero leading coefficients.
        /// coefs should be from lowest to highest degree.
        /// </summary>
        /// <param name="coefs"></param>
        public Polynomial(Rational[] coefs)
        {

            while (coefs.Length > 1 && coefs[coefs.Length - 1].Equals(Rational.Zero()))
            {
                Rational[] truncated = new Rational[coefs.Length - 1];
                for (int i = 0; i < coefs.Length-1; i++)
                    truncated[i] = coefs[i];
                coefs = truncated;
            }

            coefficients = coefs;
            degree = coefs.Length-1;
        }

        /// <summary>
        /// Creates a single term Polynomial of the form: aX^degree
        /// </summary>
        /// <param name="a"></param>
        /// <param name="degree"></param>
        public Polynomial(Rational a, int degree)
        {

            coefficients = new Rational[degree + 1];

            for (int i = 0; i < degree + 1;i++ )
            {
                if (i != degree)
                    coefficients[i] = Rational.Zero();
                else
                    coefficients[i] = a;
            }

            this.degree = degree;
        }

        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator +(Polynomial p , Polynomial q)
        {
            int deg = Math.Max(p.degree,q.degree);
            Rational[] nc = new Rational[deg+1];
            for (int i = 0; i <= deg; i++)
            {
                nc[i] = 0;
            }
            for (int i = 0; i <= deg; i++)
            {
                if (i < p.degree + 1)
                    nc[i] += p[i];
                if (i < q.degree + 1)
                    nc[i] += q[i];
            }
            return new Polynomial(nc);
        }

        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator *(Rational r, Polynomial q)
        {
            int deg = q.degree;
            Rational[] nc = new Rational[deg + 1];
            for (int i = 0; i <= deg; i++)
            {
                nc[i] = 0;
            }
            for (int i = 0; i <= deg; i++)
            {
                nc[i] += r*q[i];
            }
            return new Polynomial(nc);
        }


        /// <summary>
        /// Subtracts two polynomials
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator -(Polynomial p, Polynomial q)
        {
            int deg = Math.Max(p.degree, q.degree);
            Rational[] nc = new Rational[deg + 1];
            for (int i = 0; i <= deg; i++)
            {
                nc[i] = 0;
            }
            for (int i = 0; i <= deg; i++)
            {
                if (i < p.degree + 1)
                    nc[i] += p[i];
                if (i < q.degree + 1)
                    nc[i] -= q[i];
            }
            return new Polynomial(nc);
        }

        /// <summary>
        /// Multiplies two polynomials together.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator *(Polynomial p, Polynomial q)
        {
            int deg = p.degree+ q.degree;
            Rational[] nc = new Rational[deg + 1];
            for (int i = 0; i < deg + 1; i++)
            {
                nc[i] = 0;
            }
            for (int k = 0; k < deg+1; k++)
            {
                for (int i = 0; i <= k; i++)
                {
                    nc[k] += p[i] * q[k - i];
                }
            }
            return new Polynomial(nc);
        }
        /// <summary>
        /// Returns the quotient of division by the euclidean division.
        /// This will hide any remainder if it exists.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator /(Polynomial p, Polynomial q)
        {
            var res = Polynomial.EuclideanDivision(p, q);
            return res[0];
        }

        /// <summary>
        /// Returns the quotient of division by the euclidean division.
        /// This will hide any remainder if it exists.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator /(Polynomial p, Rational q)
        {
            Rational[] coefs = new Rational[p.coefficients.Length];
            for (int i = 0; i < p.coefficients.Length;i++ )
                coefs[i] = p[i] / q;
            return new Polynomial(coefs); 
        }


        /// <summary>
        /// Computes the remainder of P/Q
        /// </summary>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial operator %(Polynomial p, Polynomial q)
        {
            var res = Polynomial.EuclideanDivision(p, q);
            return res[1];
        }

        /// <summary>
        /// Allows us to access the i'th coefficient
        /// If you try to access a coefficients > the degree of the polynomial
        /// this will return 0.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Rational this[int index]{
            get{
                if (index >= coefficients.Length)
                {
                    return 0;
                }
                return coefficients[index];
            }
            set{
                coefficients[index]=value;
            }
        }

        /// <summary>
        /// Prints the polynomial as a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "";

            for (int i = 0; i < degree+1; i++)
            {
                if (!coefficients[i].Equals(new Rational(0)))
                { 
                    ret += coefficients[i].ToString() + "X^" + (i);
                    if (i != degree)
                    {
                            ret += " + ";

                    }

                }
            }
            return ret;
        }

        /// <summary>
        /// Compares polynomials for equality by looking at the coefficients
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Polynomial))
                return false;
            else
            {
                Polynomial temp = (Polynomial)obj;
                return this.coefficients.Intersect(temp.coefficients).Count() == this.coefficients.Union(temp.coefficients).Count();
            }
        }

        /// <summary>
        /// Formally differentiates the polynomial
        /// </summary>
        /// <returns></returns>
        public Polynomial Diff()
        {
            Rational[] derivative = new Rational[this.coefficients.Length - 1];
            for (int i = 1; i < this.coefficients.Length; i++)
            {
                derivative[i - 1] = i * this[i];
            }

            return new Polynomial(derivative);
        }

        /// <summary>
        /// Returns the coefficient of the highest power term.
        /// </summary>
        /// <returns></returns>
        public Rational LeadingTerm()
        {
            return this[degree];
        }

        /// <summary>
        /// Returns the constant term of the polynomial
        /// </summary>
        /// <returns></returns>
        public Rational ConstantTerm()
        {
            //if (this.coefficients.Length >= 1)
                return this[0];
        }

        /// <summary>
        /// Returns true is this polynomial is a constant
        /// </summary>
        /// <returns></returns>
        public bool IsConstant()
        {
            return (this.coefficients.Length == 1) || (this.coefficients.Length == 0);
        }

        /// <summary>
        /// Returns the content of a polynomial
        /// (The GCD of the coefficients)
        /// </summary>
        /// <returns></returns>
        public Rational Cont()
        {
            return Utils.PolynomialContent(this.coefficients);
        }

        /// <summary>
        /// Calculates the discriminant of the polynomial
        /// </summary>
        /// <returns></returns>
        public Rational Discriminant()
        {
            return Utils.PolynomialDiscriminant(this.coefficients);
        }

        /// <summary>
        /// Returns an array containing the result and the remainder
        /// returns Q and R such that A = BQ+R.
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Polynomial[] EuclideanDivision(Polynomial A,Polynomial B){
            Polynomial[] results = new Polynomial[2];
            Polynomial R = new Polynomial(A.coefficients); // R<- A
            Polynomial Q = new Polynomial(new Rational[1] { 0 }); //Q<-0

            while (R.degree >= B.degree)
            {
                //Coefficients for S
                Rational[] sc = new Rational[(R.degree - B.degree)+1];
                for (int i = 0; i <= R.degree - B.degree; i++)
                    sc[i] = 0;
                Rational co = R.LeadingTerm() / B.LeadingTerm();
                sc[(R.degree - B.degree)] = co;
                Polynomial S = new Polynomial(sc);
                Q = Q + S;
                R = R - (S * B);
            }

            results[0] = Q;
            results[1] = R;
            return results;
        }

        /// <summary>
        /// This assumes that you can write X^n = P(1,x,...,x^n-1)
        /// and reduces the current polynomial by that identity
        /// </summary>
        /// <param name="p"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public Polynomial Reduce(Polynomial p, int n)
        {
            //We have that X^n = p(X)
            //TODO: this is just the remainder of the current polynomial / p ?

            var coefs = (Rational[])this.coefficients.Clone();
            var temp = new Polynomial(coefs);

            while (temp.degree >= n)
            {
                int deg = temp.degree;
                Rational coef = temp[deg];
                var poly = new Polynomial(coef, deg-n);
                temp = temp - new Polynomial(coef, deg);
                temp = temp + (p * poly);

            }

            return temp;
        }
        /// <summary>
        /// Returns the degree of the current polynomial instance
        /// </summary>
        public int Degree
        {
            get { return degree; }
        }

        /// <summary>
        /// Returns the coefficient array of the current polynomial instance
        /// </summary>
        public Rational[] Coefficients
        {
            get { return this.coefficients; }
        }



        /// <summary>
        /// Computes the polynomial GCD
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Polynomial GCD(Polynomial A, Polynomial B)
        {
            if (B.IsConstant())
                return A;
            while (!B.IsConstant())
            {
                Polynomial temp = new Polynomial(B.coefficients);
                B = A % B;
                A = temp;
            }
            return A;
        }

        /// <summary>
        /// Computes the companion matrix of the current polynomial.
        /// Will be a large sparse matrix, however we don't support optimizations for sparce matricies
        /// So this will be a very large memory sink. 
        /// </summary>
        /// <returns></returns>
        public Matrix CompanionMatrix()
        {
            Rational[,] mat = new Rational[this.degree, this.degree];
            for (int row = 0; row < this.degree; row++)
            {
                for (int col = 0; col < this.degree; col++)
                {
                    if (row - 1 >= 0)
                    {
                        if (row - 1 == col)
                        {
                            mat[row, col] = Rational.One();
                            continue;
                        }
                    }
                    if (col == degree-1)
                        mat[row, col] = -1 * coefficients[row] / LeadingTerm();
                    else
                        mat[row, col] = Rational.Zero();
                }
            }

            return new Matrix(mat);
        }

        

        /// <summary>
        /// Evaluate the polynomial at a given complex number z.
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        public Complex Eval(Complex z)
        {
            Complex result = 0;

            for (int i = 0; i <= this.degree; i++)
            {
                result += this[i] * Complex.Pow(z,i) ;
            }

            return result;
        }

        /// <summary>
        /// Evaluate the polynomial with a given Matrix as the variable
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public Matrix Eval(Matrix A)
        {
            Matrix result = Matrix.ZeroMatrix(A.Rows());
            
            for (int i = 0; i <= this.degree; i++)
            {
                result += this[i] * (A^i);
            }

            return result;
        }


    }
}
