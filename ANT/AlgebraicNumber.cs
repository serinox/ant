﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;



namespace ANT
{
    public class AlgebraicNumber
    {
        Rational[] Ai;
        NumberField ParentField;


        public AlgebraicNumber(Rational[] ai, NumberField parent)
        {
            this.Ai = ai;
            this.ParentField = parent;
        }

        public static AlgebraicNumber operator +(AlgebraicNumber x, AlgebraicNumber y){
            if (x.ParentField != y.ParentField)
                throw new ArithmeticException();
            
            Rational[] tmp = new Rational[x.Dim];
            for (int i = 0; i < x.Dim; i++){
                tmp[i] = x.Ai[i] + y.Ai[i];
            }
            return new AlgebraicNumber(tmp,x.ParentField);
        }

        private int Dim
        {
            get { return this.ParentField.Dimension; }
        }

        private Complex theta
        {
            get { return this.ParentField.Theta; }
        }


        public static AlgebraicNumber operator -(AlgebraicNumber x, AlgebraicNumber y)
        {
            if (x.ParentField != y.ParentField)
                throw new ArithmeticException();

            Rational[] tmp = new Rational[x.Dim];
            for (int i = 0; i < x.ParentField.Dimension; i++)
            {
                tmp[i] = x.Ai[i] - y.Ai[i];
            }
            return new AlgebraicNumber(tmp,x.ParentField);
        }

        public static AlgebraicNumber operator *(AlgebraicNumber x, AlgebraicNumber y)
        {

            if (x.ParentField != y.ParentField)
                throw new ArithmeticException();

            Polynomial xpoly = new Polynomial(x.Ai);
            Polynomial ypoly = new Polynomial(y.Ai);
            Polynomial anspoly = xpoly * ypoly;
            anspoly = anspoly.Reduce(x.ParentField.ReductionPolynomial, x.ParentField.Dimension);
            return new AlgebraicNumber(anspoly.Coefficients,x.ParentField);

        }

        public Complex eval()
        {
            Complex res = new Complex(0, 0);

            for (int i = 0; i < Dim; i++)
            {
                res += Complex.Pow(theta, i) * Ai[i];
            }

            return res;
        }

        public override string ToString()
        {
            string ret = "";

            for (int i = 0; i < Dim; i++)
            {
                ret += Ai[i].ToString() + "T^" + (i);
                if (i != Dim - 1)
                {
                    ret += " + ";
                }
            }


            return ret;
        }
        public Rational Norm()
        {
            Rational ret = 1;

            //Compute the resultant between the polynomial defining the number field primitive element
            //and the polynomial in the power basis defining the current element.
            ret = Utils.Resultant(this.ParentField.MinimalPolynomial.Coefficients, this.Ai);


            return ret;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AlgebraicNumber))
                return false;
            else
            {
                
                AlgebraicNumber other = (AlgebraicNumber)obj;
                if (other.ParentField != this.ParentField)
                    return false;
                if (!Enumerable.SequenceEqual(other.Ai,this.Ai))
                    return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
