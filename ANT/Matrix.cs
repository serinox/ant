﻿using System;
using System.Collections.Generic;

namespace ANT
{
    public class Matrix
    {
        
        private readonly uint _sRows;
        private readonly uint _sColumns;
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private Rational[,] _matrixData;

        private LUDecomposition _lud;
        //		private CholeskyDecomposition<T> CholD;
        //		private SingularValueDecomposition<T> SVD;
        //		private EigenvalueDecomposition<T> EVD;

        /// <summary>
        /// default constuctor.
        /// </summary>
        private Matrix()
        {
            
        }
        /// <summary>
        /// Constructor for list of list
        /// </summary>
        /// <param name="rows"></param>
        public Matrix(List<List<Rational>> rows)
            : this()
        {
            _sRows = (uint)rows.Count;
            if (_sRows != 0)
                _sColumns = (uint)rows[0].Count;
            else
                _sColumns = 0;

            _matrixData = new Rational[_sRows, _sColumns];
            for (int i = 0; i < _sRows; i++)
            {
                List<Rational> rowi = rows[i];
                for (int j = 0; j < _sColumns; j++)
                {
                    _matrixData[i, j] = rowi[j];
                }
            }

        }
        /// <summary>
        /// Constructor for array of arrays
        /// </summary>
        /// <param name="m"></param>
        public Matrix(Rational[][] m)
            : this()
        {
            _sRows = (uint)m.GetLength(0);
            _sColumns = (uint)m[0].GetLength(0);
            _matrixData = new Rational[_sRows, _sColumns];

            for (int i = 0; i < _sRows; i++)
                for (int j = 0; j < _sColumns; j++)
                    _matrixData[i, j] = m[i][j];
        }
        /// <summary>
        /// Constructor for multidimensional Array
        /// </summary>
        /// <param name="m"></param>
        public Matrix(Rational[,] m)
            : this()
        {
            _sRows = (uint)m.GetLength(0);
            _sColumns = (uint)m.GetLength(1);
            _matrixData = m;
        }
        /// <summary>
        /// Matrix addition
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Matrix operator +(Matrix a, Matrix b)
        {
            if (a._sRows != b._sRows || a._sColumns != b._sColumns)
                throw new InvalidOperationException("these matrices have different sizes so addition is not defined");

            var rows = (int)a._sRows;
            var cols = (int)a._sColumns;
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {

                for (int j = 0; j < cols; j++)
                {
                    mData[i, j] = a[i, j]+ b[i, j];
                }
            }

            return new Matrix(mData);
        }
        /// <summary>
        /// Matrix Subtraction
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Matrix operator -(Matrix a, Matrix b)
        {
            if (a._sRows != b._sRows || a._sColumns != b._sColumns)
                throw new InvalidOperationException("these matrices have different sizes so subtraction is not defined");

            var rows = (int)a._sRows;
            var cols = (int)a._sColumns;
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {

                for (int j = 0; j < cols; j++)
                {
                    mData[i, j] = a[i, j]- b[i, j];
                }
            }

            return new Matrix(mData);
        }

        /// <summary>
        /// Returns a hash code representation
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }
        /// <summary>
        /// Checks whether the diagonals are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DiagEquals(object obj)
        {
            var other = obj as Matrix;
            if (other != null && (_sColumns != other._sColumns || _sRows != other._sRows))
                return false;
            var tmp = _sRows;
            for (int i = 0; i < tmp; i++)
            {

                if (other != null && !(this[i, i]== other[i, i]))
                    return false;

            }

            return true;
        }

        /// <summary>
        /// Returns the given matrix reformatted as a column vector.
        /// </summary>
        /// <returns></returns>
        public Rational[] AsColumn()
        {
            Rational[] mcol = new Rational[_sRows * _sColumns];

            for (int row = 0; row < _sRows; row++)
            {
                for (int col = 0; col < _sColumns; col++)
                {
                    mcol[col + row * _sColumns] = this[row, col];
                }
            }

            return mcol;
        }

        /// <summary>
        /// Sets the given column (0-indexed) with the coefficients given in the array
        /// </summary>
        public void SetColumn(Rational[] entries, int col)
        {
            for (int i = 0; i < _sRows; i++)
            {
                this[i, col] = entries[i];
            }
        }

        public Rational DiagProduct()
        {
            if (_sColumns != _sRows)
            {
                Console.WriteLine("DiagProduct - not supported on non-square matrices");
                return Rational.Zero();
            }
            Rational res = Rational.One();

            for (int i = 0; i < _sColumns; i++)
            {
                res = (res* _matrixData[i, i]);
            }

            return res;
        }

        /// <summary>
        /// Element wise comparasion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var other = obj as Matrix;
            if (other != null && (_sColumns != other._sColumns || _sRows != other._sRows))
                return false;

            for (int i = 0; i < _sRows; i++)
            {
                for (int j = 0; j < _sColumns; j++)
                {

                    if (other != null && !(this[i, j]== other[i, j]))
                        return false;
                }
            }

            return true;
        }
        /// <summary>
        /// Returns a Square zero Matrix
        /// </summary>
        public static Matrix ZeroMatrix(int rows, int cols)
        {
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    mData[i, j] = Rational.Zero();
                }
            }
            return new Matrix(mData);
        }
        /// <summary>
        /// Returns a Square zero Matrix
        /// </summary>
        public static Matrix ZeroMatrix(int size)
        {
            var mData = new Rational[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    mData[i, j] = Rational.Zero();
                }
            }
            return new Matrix(mData);
        }

        /// <summary>
        /// Augments a matrix by appending columns from another matrix
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Matrix Augment(Matrix a, Matrix b)
        {
            if (a._sRows != b._sRows)
                throw new InvalidOperationException("can't augment matrices with differing number of rows");

            var nm = new Rational[a._sRows, a._sColumns + b._sColumns];

            for (int i = 0; i < a._sRows; i++)
            {
                for (int j = 0; j < a._sColumns; j++)
                {
                    nm[i, j] = a._matrixData[i, j];
                }
                for (uint j = a._sColumns; j < a._sColumns + b._sColumns; j++)
                {
                    nm[i, j] = b._matrixData[i, j - a._sColumns];
                }
            }

            return new Matrix(nm);
        }

        /// <summary>
        /// Finds the inverse of a matrix through Gaussian Elimination
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix Inverse(Matrix m)
        {
            //			if (m._sColumns != m._sRows)
            //				throw new NotImplementedException ("hahahaha that matrix aint square and we don't do psuedo investments");
            //
            //			var augm = Augment (m, Identity ((int)m._sRows));
            //			augm = RowReducedEchelonForm (augm);
            //			var inv = new T[m._sRows,m._sRows];
            //			for (int i = 0; i < m._sRows; i++) 
            //			{
            //				for (int j = 0; j < m._sRows; j++) 
            //				{
            //					inv [i, j] = augm [i, j + (int)m._sRows];
            //				}
            //			}
            //
            //			return new Matrix<T,C>(inv);
            return m.Solve(Identity(m.Rows()));
        }
        /// <summary>
        /// Returns the element wise conjugate of the matrix
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix Conjugate(Matrix m)
        {
            var mt = new Rational[m._sColumns, m._sRows];
            for (int i = 0; i < m._sColumns; i++)
            {
                for (int j = 0; j < m._sRows; j++)
                {
                    mt[i, j] = m._matrixData[i, j];
                }
            }
            return new Matrix(mt);
        }
        /// <summary>
        /// Transposes the matrix
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix Transpose(Matrix m)
        {
            var mt = new Rational[m._sColumns, m._sRows];
            for (int i = 0; i < m._sColumns; i++)
            {
                for (int j = 0; j < m._sRows; j++)
                {
                    mt[i, j] = m._matrixData[j, i];
                }
            }
            return new Matrix(mt);
        }
        /// <summary>
        /// Returns the size x size Identity matrix
        /// </summary>
        public static Matrix Identity(int size)
        {
            var mData = new Rational[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == j)
                        mData[i, j] = Rational.One();
                    else
                        mData[i, j] = Rational.Zero();
                }
            }
            return new Matrix(mData);
        }

        /// <summary>
        /// Performs the elementary row operation 
        /// r1 = r1 + factor*r2
        /// 
        /// </summary>
        public Matrix ElemRowAdd(int r1, int r2, Rational factor)
        {
            var nData = new Rational[_sRows, _sColumns];

            Array.Copy(_matrixData, nData, _matrixData.Length);
            for (int i = 0; i < _sColumns; i++)
            {
                nData[r1, i] = (nData[r1, i]+ (factor* nData[r2, i]));
            }

            return new Matrix(nData);
        }
        /// <summary>
        /// Subtract a scaled row from another row
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        private void DirectElemRowSub(int r1, int r2, Rational factor)
        {
            int l = _matrixData.GetLength(1);
            for (int j = 0; j < l; j++)
            {
                _matrixData[r1, j] = (_matrixData[r1, j]- (factor* _matrixData[r2, j]));
            }
        }
        /// <summary>
        /// Subtract a scaled row from another row
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public Matrix ElemRowSub(int r1, int r2, Rational factor)
        {
            var nData = new Rational[_sRows, _sColumns];

            Array.Copy(_matrixData, nData, _matrixData.Length);
            for (int i = 0; i < _sColumns; i++)
            {
                nData[r1, i] = (nData[r1, i]- (factor* nData[r2, i]));
            }

            return new Matrix(nData);
        }

        /// <summary>
        /// This normalizes the first nonzero element in the row.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="nonzero">The index of the first nonzero element in the row</param>
        public void DirectElemRowScalDiv(int row, int nonzero)
        {
            var nonz = _matrixData[row, nonzero];
            for (int i = 0; i < _sColumns; i++)
            {
                _matrixData[row, i] = (_matrixData[row, i]/ nonz);
            }
        }

        /// <summary>
        /// Scales a row by division of a scalar.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="factor"></param>
        public void DirectElemRowScalDiv(int row, Rational factor)
        {

            for (int i = 0; i < _sColumns; i++)
            {
                _matrixData[row, i] = (_matrixData[row, i]/ factor);
            }


        }
        /// <summary>
        /// Divide a row by a factor
        /// </summary>
        /// <param name="row"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public Matrix ElemRowScalDiv(int row, Rational factor)
        {
            var nData = new Rational[_sRows, _sColumns];

            Array.Copy(_matrixData, nData, _matrixData.Length);
            for (int i = 0; i < _sColumns; i++)
            {
                nData[row, i] = (nData[row, i]/ factor);
            }

            return new Matrix(nData);
        }

        /// <summary>
        /// Scale a row by a factor
        /// </summary>
        /// <param name="row"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public Matrix ElemRowScal(int row, Rational factor)
        {
            var nData = new Rational[_sRows, _sColumns];

            Array.Copy(_matrixData, nData, _matrixData.Length);
            for (int i = 0; i < _sColumns; i++)
            {
                nData[row, i] = (factor* nData[row, i]);
            }

            return new Matrix(nData);
        }

        /// <summary>
        /// Swap two rows.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public Matrix ElemRowSwap(int r1, int r2)
        {
            var mData = new Rational[_sRows, _sColumns];
            for (int i = 0; i < _sRows; i++)
            {
                for (int j = 0; j < _sColumns; j++)
                {
                    if (i == r1)
                        mData[r2, j] = _matrixData[r1, j];
                    if (i == r2)
                        mData[r1, j] = _matrixData[r2, j];
                    else
                        mData[i, j] = _matrixData[i, j];
                }
            }
            return new Matrix(mData);
        }

        /// <summary>
        /// First nonzero element in the row specified
        /// </summary>
        /// <param name="m"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private static int FirstNonZero(ref Matrix m, int row)
        {
            for (int i = 0; i < m._sColumns; i++)
            {
                if (!(m._matrixData[row, i]== Rational.Zero()))
                    return i;
            }
            return -1;
        }
        /// <summary>
        /// First nonzero element in the list.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        private static int FirstNonZero(Rational[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                if (!(v[i]== Rational.Zero()))
                    return i;
            }
            return -1;
        }
        /// <summary>
        /// Gaussian Elimination with 1's as the leading entries 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix RowReducedEchelonForm(Matrix m)
        {
            m = new Matrix(m._matrixData);
            for (int i = 0; i < m._sRows; i++)
            {
                //Console.WriteLine (i);
                var nonz = FirstNonZero(ref m, i);
                m.DirectElemRowScalDiv(i, nonz);
                for (int j = 0; j < m._sRows; j++)
                {
                    if (j != i || (m._matrixData[j, i]== Rational.Zero()))
                    {
                        nonz = FirstNonZero(ref m, i);
                        Rational factor = (m._matrixData[j, i]/ m._matrixData[i, nonz]);
                        m.DirectElemRowSub(j, i, factor);

                    }
                }
            }
            return m;
        }
        /// <summary>
        /// Performs Guassian Elimination
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix RowReducedForm(Matrix m)
        {
            
            for (int i = 0; i < m._sRows; i++)
            {
                //Console.WriteLine (i);
                for (int j = i; j < m._sRows; j++)
                {
                    if (j != i)
                    {
                        var nonz = FirstNonZero(m.GetRow(i));
                        if (nonz == -1)
                            continue;
                        Rational factor = (m.GetRow(j)[i]/ m.GetRow(i)[nonz]);
                        m.DirectElemRowSub(j, i, factor);
                        //Console.WriteLine(m);
                    }
                    else
                    {
                        var nonz = FirstNonZero(m.GetRow(i));
                        if (nonz == -1)
                            continue;
                        m.DirectElemRowScalDiv(i, m.GetRow(i)[nonz]);
                        //Console.WriteLine(m);
                    }
                }
            }
            return m;
        }
        /// <summary>
        /// Converts the matrix to an Array of Arrays.
        /// </summary>
        /// <returns></returns>
        public Rational[][] ToJagArray()
        {
            var ret = new Rational[_sRows][];
            for (int i = 0; i < _sRows; i++)
            {
                ret[i] = GetRow(i);
            }

            return ret;
        }

        /// <summary>
        /// Returns the inner array representation of the matrix
        /// </summary>
        /// <returns></returns>
        public Rational[,] ToArray()
        {
            return _matrixData;
        }
        /// <summary>
        /// Returns the number of columns in the matrix
        /// </summary>
        /// <returns></returns>
        public int Columns()
        {
            return (int)_sColumns;
        }

        /// <summary>
        /// Returns the number of rows in the matrix
        /// </summary>
        /// <returns></returns>
        public int Rows()
        {
            return (int)_sRows;
        }

        /// <summary>
        /// Finds the determinant through the LU decomposition
        /// </summary>
        /// <returns></returns>
        public Rational Determinant()
        {
            return LUDecompose().Determinant();
        }

        /// <summary>
        /// Returns the LU Decomposition of a matrix
        /// </summary>
        /// <returns></returns>
        public LUDecomposition LUDecompose()
        {
            return _lud ?? (_lud = new LUDecomposition(this));
        }


        //TODO

        //		public  CholeskyDecomposition chol()
        //		{
        //			return new CholeskyDecomposition(this);
        //		}
        //		public  SingularValueDecomposition SVD()
        //		{
        //			return new SingularValueDecomposition(this);
        //		}

        

        /// <summary>
        /// Computes the coefficients of the monic Characteristic Polynomial.
        /// Coefficients are from lowest to highest degree.
        /// </summary>
        /// <returns></returns>
        public Rational[] CharacteristicPolynomialCoefficients()
        {
            int n = this.Rows();
            Matrix C = Identity(n);
            Rational[] a = new Rational[n + 1];
            a[0] = Rational.One();
            for (int i = 1; i <= n; i++)
            {
                if (i == n)
                {
                    a[i] = -1*(((this * C).Trace()/ ((Rational)i)));
                }
                C = this * C;
                a[i] = -1*(((C).Trace()/ ((Rational)i)));
                C = C + (a[i] * Identity(n));
            }
            Array.Reverse(a);
            return a;
        }

        /*/// <summary>
        /// Computes the coefficients of the minimal polynomial of the current matrix
        /// </summary>
        /// <returns></returns>
        public Rational[] MinimalPolynomialCoefficients()
        {
            var bmat = Matrix.ZeroMatrix((this.Rows()) * (this.Rows()), this.Rows() + 1);
            var temp = new Matrix((Rational[,])this._matrixData.Clone());
            for (int i = 1; i < Rows(); i++)
            {
                var tcol = temp.AsColumn();
                bmat.SetColumn(tcol, i);
                temp = temp * temp;
            }
            Console.WriteLine(bmat);
            return null;
        }*/

        //
        //		public  int Rank()
        //		{
        //			return new SingularValueDecomposition(this).Rank();
        //		}
        //		public  double Condition()
        //		{
        //			return new SingularValueDecomposition(this).Condition();
        //		}

        /// <summary>
        /// Trace of a matrix.
        /// </summary>
        /// <returns></returns>
        public Rational Trace()
        {
            Rational t = Rational.Zero();
            for (int i = 0; i < Math.Min(Rows(), Columns()); i++)
            {
                t = (t+ _matrixData[i, i]);
            }
            return t;
        }

        /// <summary>
        /// Matrix Multiplication.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a._sColumns != b._sRows)
                throw new InvalidOperationException("these matrices are not of the right size to multiply together");


            var rows = (int)a._sRows;
            var cols = (int)b._sColumns;
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                Rational[] row = a.GetRow(i);
                for (int j = 0; j < cols; j++)
                {
                    Rational[] col = b.GetCol(j);
                    mData[i, j] = DotProduct(row, col);
                }
            }

            return new Matrix(mData);
        }

        /// <summary>
        /// Solves the system for square matrices using inv(A)*x
        /// this will be slow for large matrices
        /// this will be nonaccurate for some matrices
        /// this is not the best way to do this
        /// stop and think about what you are doing...
        /// UPDATE:Use Solve. 
        /// </summary>
        /// <param name="a">A matrix </param>
        /// <param name="x">this is either a matrix or a vector.</param>
        public static Matrix SolveSystem(Matrix a, Matrix x)
        {
            var ainv = Inverse(a);
            return ainv * x;
        }

        

        

        /// <summary>
        /// Uses LU Decomposition to solve a Linear System.
        /// Requires Square Matrix?
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        public Matrix SolveVector(Matrix z)
        {

            return LUDecompose().SolveVector(z);
        }

        /// <summary>
        /// Solves Ax = B
        /// using QR or LU decompositions
        /// </summary>
        /// <param name="B">B.</param>
        // ReSharper disable once InconsistentNaming
        public Matrix Solve(Matrix B)
        {
            var square = Columns() == Rows();
            if (square)
                return LUDecompose().Solve(B);
            return null;
        }

        // ReSharper disable once InconsistentNaming
        public Matrix SolveTranspose(Matrix B)
        {
            return Transpose(this).Solve(Transpose(B));
        }
        /// <summary>
        /// Returns alpha *x + y. where x,y vectors and alpha is a element of the field.
        /// </summary>
        public static Rational[] AlphaXPlusY(Rational alpha, Rational[] row, Rational[] col)
        {
            if (row.Length != col.Length)
                throw new InvalidOperationException("Woah there, thems not the same length try again.");
            int l = row.Length;
            var result = new Rational[l];

            for (int i = 0; i < l; i++)
            {
                result[i] = ((row[i]* alpha)+ col[i]);
            }
            return result;
        }
        /// <summary>
        /// Performs a DotProduct between a row vector and a column vector. 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static Rational DotProduct(Rational[] row, Rational[] col)
        {
            if (row.Length != col.Length)
                throw new InvalidOperationException("Woah there, thems not the same length try again.");
            int l = row.Length;
            Rational result = Rational.Zero();

            for (int i = 0; i < l; i++)
            {
                result = (result+ (row[i]* col[i]));
            }
            return result;
        }

        /// <summary>
        /// Returns a column of the matrix
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Rational[] GetCol(int i)
        {
            var col = new Rational[_sRows];
            for (int j = 0; j < _sRows; j++)
            {
                col[j] = _matrixData[j, i];
            }
            return col;
        }
        /// <summary>
        /// Returns a row of the matrix
        /// </summary>
        /// <param name="i"></param>
        /// <param name="row"></param>
        public void DirectGetRow(int i, ref Rational[] row)
        {
            if (row == null)
            {
                row = new Rational[_sColumns];
            }

            for (int j = 0; j < _sColumns; j++)
            {
                row[j] = _matrixData[i, j];
            }

        }

        /// <summary>
        /// Returns a row of the matrix
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Rational[] GetRow(int i)
        {
            var row = new Rational[_sColumns];
            for (int j = 0; j < _sColumns; j++)
            {
                row[j] = _matrixData[i, j];
            }
            return row;
        }

        /// <summary>
        /// Matrix times a scalar on the left.
        /// </summary>
        /// <param name="b"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Matrix operator *(Rational b, Matrix a)
        {

            var rows = (int)a._sRows;
            var cols = (int)a._sColumns;
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {

                for (int j = 0; j < cols; j++)
                {
                    mData[i, j] = (a[i, j]* b);
                }
            }

            return new Matrix(mData);
        }

        /// <summary>
        /// Matrix raised to an integer power
        /// </summary>
        /// <param name="b"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Matrix operator ^(Matrix a, int pow)
        {
            if (!a.IsSquare())
            {
                throw new ArithmeticException("This is not square so cannot exponentiate.");
            }

            if (pow == 0)
                return Matrix.Identity(a.Rows());

            var tmp = new Matrix((Rational[,])a._matrixData.Clone());

            if (pow == 1)
                return tmp;

            for (int i = 1; i < pow; i++)
                tmp = tmp * a;
            return tmp;
        }




        /// <summary>
        /// Returns true if the matrix is Square.
        /// </summary>
        /// <returns></returns>
        public Boolean IsSquare()
        {
            return this.Rows() == this.Columns();
        }

        /// <summary>
        /// Matrix times a scalar on the right
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Matrix operator *(Matrix a, Rational b)
        {

            var rows = (int)a._sRows;
            var cols = (int)a._sColumns;
            var mData = new Rational[rows, cols];
            for (int i = 0; i < rows; i++)
            {

                for (int j = 0; j < cols; j++)
                {
                    mData[i, j] = (a[i, j]* b);
                }
            }

            return new Matrix(mData);
        }

        /// <summary>
        /// Access an element of the Matrix
        /// </summary>
        /// <param name="i1"></param>
        /// <param name="i2"></param>
        /// <returns></returns>
        public Rational this[int i1, int i2]
        {
            get
            {
                if (i1 > _sRows || i2 > _sColumns)
                    throw new IndexOutOfRangeException("Woah there, your requested index aint in this here matrix; check the size and try again.");
                return _matrixData[i1, i2];
            }
            set
            {
                if (i1 > _sRows || i2 > _sColumns)
                    throw new IndexOutOfRangeException("Woah there, your requested index aint in this here matrix; check the size and try again.");
                _matrixData[i1, i2] = value;
            }
        }


        /// <summary>
        /// Computes the Kronecker Product of two matrices.
        /// This is equivalent to the tensor product of the linear transformations
        /// given by the two matricies.
        /// 
        /// This is a very slow process for large matricies. for 2 nxn matricies its something like n^4 operations and n^4 memory
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Matrix KroneckerProduct(Matrix A, Matrix B)
        {
            int res_cols = A.Columns() * B.Columns();
            int res_rows = A.Rows() * B.Rows();
            Rational[,] resultdata = new Rational[res_rows, res_cols];
            for (int arow = 0; arow < A.Rows(); arow++)
            {
                for (int acol = 0; acol < A.Columns(); acol++)
                {
                    for (int brow = 0; brow < B.Rows(); brow++)
                    {
                        for (int bcol = 0; bcol < B.Columns(); bcol++)
                        {
                            //Top corner is arow*B.Rows(),acol*B.Columns();
                            resultdata[arow * B.Rows() + brow, acol * B.Columns() + bcol] = (A[arow, acol]* B[brow, bcol]);
                        }
                    }
                }
            }


            return new Matrix(resultdata);
        }


        /// <summary>
        /// Returns a tuple of (Rows,Columns)
        /// </summary>
        /// <returns></returns>
        public Tuple<uint, uint> GetSize()
        {
            return new Tuple<uint, uint>(_sRows, _sColumns);
        }


        /// <summary>
        /// Returns a multiline string representation of the matrix
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "Matrix :" + Environment.NewLine;
            for (int i = 0; i < _sRows; i++)
            {
                ret += "[";
                for (int j = 0; j < _sColumns; j++)
                {
                    ret += _matrixData[i, j].ToString();
                    if (j != (int)_sColumns - 1)
                        ret += ", ";
                }
                ret += "]";
                ret += Environment.NewLine;
            }
            return string.Format(ret);
        }

        /// <summary>
        /// Gets the submatrix as permuted by vector r.
        /// </summary>
        /// <returns>The matrix.</returns>
        /// <param name="r">The permutation vector</param>
        /// <param name="j0">J0.</param>
        /// <param name="j1">J1.</param>
        public Matrix GetMatrix(int[] r, int j0, int j1)
        {
            var x = new Rational[r.Length, j1 - j0 + 1];
            for (int i = 0; i < r.Length; i++)
                for (int j = j0; j <= j1; j++)
                    x[i, j - j0] = _matrixData[r[i], j];

            return new Matrix(x);
        }
    }
}

