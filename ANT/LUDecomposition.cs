﻿using System;


namespace ANT
{
    public class LUDecomposition
    {
        

        //Array for internal storage of decomposition. 
        //TODO make private again.
        public Rational[,] LU;

        //Row and column dimensions, and pivot sign.
        private readonly int _m;
        private readonly int _n;
        private readonly int _pivsign;

        //storage of pivot vector.
        private readonly int[] _piv;

        private LUDecomposition()
        {
        }
        // ReSharper disable once InconsistentNaming
        public LUDecomposition(Matrix A)
            : this()
        {
            LU = (Rational[,])A.ToArray().Clone();
            _m = A.Rows();
            _n = A.Columns();
            if (_m != _n)
                throw new NotSupportedException("not square!");
            _piv = new int[_m];
            for (int i = 0; i < _m; i++)
            {
                _piv[i] = i;
            }

            _pivsign = 1;


            for (int j = 0; j < _n; j++)
            {

                for (int i = 0; i < _m; i++)
                {
                    int kmax = Math.Min(i, j);
                    Rational s = Rational.Zero();
                    for (int k = 0; k < kmax; k++)
                    {
                        s = (s+ (LU[i, k]* LU[k, j]));
                    }
                    LU[i, j] = (LU[i, j]- s);

                    LU[i, j] = LU[i, j];
                }

                // Find pivot and exchange if necessary.
                int p = j;
                for (int i = j + 1; i < _m; i++)
                {
                    if (((LU[i, j].Abs()) >(LU[p, j].Abs())))
                    {
                        p = i;
                    }
                }
                if (p != j)
                {
                    for (int k = 0; k < _n; k++)
                    {
                        Rational t = LU[p, k];
                        LU[p, k] = LU[j, k];
                        LU[j, k] = t;
                    }
                    int k2 = _piv[p];
                    _piv[p] = _piv[j];
                    _piv[j] = k2;
                    _pivsign = -_pivsign;
                }
                if (j < _m & !(LU[j, j]== Rational.Zero()))
                {
                    for (int i = j + 1; i < _m; i++)
                    {
                        LU[i, j] = (LU[i, j]/ LU[j, j]);
                    }
                }

            }

        }
        public void DirectGetRow(int i, ref Rational[] row)
        {
            if (row == null)
            {
                row = new Rational[_n];
            }

            for (int j = 0; j < _n; j++)
            {
                row[j] = LU[i, j];
            }

        }

        virtual public bool IsNonSingular
        {
            get
            {
                for (int j = 0; j < _n; j++)
                {
                    if ((LU[j, j]) == Rational.Zero())
                        return false;
                }
                return true;
            }
        }
        public override string ToString()
        {
            string ret = "Matrix :" + Environment.NewLine;
            for (int i = 0; i < _m; i++)
            {
                ret += "[";
                for (int j = 0; j < _n; j++)
                {
                    ret += LU[i, j].ToString();
                    if (j != _n - 1)
                        ret += ", ";
                }
                ret += "]";
                ret += Environment.NewLine;
            }
            return string.Format(ret);
        }
        public Rational Determinant()
        {
            if (_m != _n)
                throw new NotSupportedException("Square Matrix only");
            Rational det = _pivsign > 0 ? Rational.One() : new Rational(-1);

            for (int j = 0; j < _n; j++)
                det = (det* LU[j, j]);


            return det;
        }
        public Matrix L()
        {
            var lArr = new Rational[_m, _n];
            for (int i = 0; i < _m; i++)
            {
                for (int j = 0; j < _n; j++)
                {
                    if (i > j)
                        lArr[i, j] = LU[i, j];
                    else if (i == j)
                        lArr[i, j] = Rational.One();
                    else
                        lArr[i, j] = Rational.Zero();
                }
            }
            return new Matrix(lArr);

        }
        public Matrix P()
        {
            var arr = new Rational[_m, _m];
            for (int i = 0; i < _m; i++)
            {
                for (int j = 0; j < _m; j++)
                {
                    if (j == _piv[i])
                        arr[i, j] = Rational.One();
                    else
                        arr[i, j] = Rational.Zero();
                }
            }
            return new Matrix(arr);
        }

        public Matrix Piv()
        {
            var vals = new Rational[1, _m];
            for (int i = 0; i < _m; i++)
            {
                Rational tmp = Rational.Zero();
                try
                {
                    tmp = (Rational)Convert.ChangeType(_piv[i], typeof(Rational));
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                    //error
                    //TODO: not ignore this error;
                }

                vals[0, i] = tmp;
            }

            return new Matrix(vals);
        }

        public Matrix U()
        {
            var lArr = new Rational[_m, _n];
            for (int i = 0; i < _m; i++)
            {
                for (int j = 0; j < _n; j++)
                {
                    if (i <= j)
                        lArr[i, j] = LU[i, j];
                    else
                        lArr[i, j] = Rational.Zero();
                }
            }
            return new Matrix(lArr);

        }

        /// <summary>
        /// This will try to return a nonzero vector solving the system.
        /// </summary>
        /// <returns>The vector.</returns>
        /// <param name="B">B.</param>
        // ReSharper disable once InconsistentNaming
        public Matrix SolveVector(Matrix B)
        {
            if (B.Rows() != _m)
                throw new ArgumentException("rows don't match");
            if (B.Columns() != 1)
                throw new ArgumentException("aint a vector");
            //			if (this.IsNonSingular) //If it's not singular we  have a solve method that works just fine.
            //				return Solve (B);

            //We've pivoted our system so we need to pivot the B vector.
            //we'll undo this later (hopefully).

            var c = Matrix.Transpose(P()) * B;
            // at this point we know that the matrix is singular so we want to find something nonzero if B is the zero vector. 
            // basically we'll shove something nonzero into the free variables.

            //this will store the solution to Ly = B
            var y = new Rational[_m, 1];

            //This will get the solution to Ux = Y
            var x = new Rational[_m, 1];


            //Forward Sub L*Y = (P^T)B; 
            //By our LU factorization L will have det = 1 and not be singular.
            //For the zero vector this better be zero;
            for (int i = 0; i < _n; i++)
            {
                //i is the row. J is the column. But L is lower triangular so we only need j<=i to go up to the diagonal.
                for (int j = 0; j <= i; j++)
                {
                    if (j == i)
                        y[i, 0] = (y[i, 0]+ c[i, 0]);
                    else
                    {
                        y[i, 0] = (y[i, 0]- (LU[i, j]* y[j, 0]));
                    }

                }
            }
            //			Console.Write ("y = ");
            //			Console.WriteLine (new Matrix<T,C> (y));
            //
            //Back Sub U*x = Y;
            for (int i = _n - 1; i >= 0; i--)
            {
                for (int j = i; j < _n; j++)
                {
                    if (j == i)
                    {
                        //We have a free variable if this is zero
                        if (Rational.Zero() == (LU[i, j]) && Rational.Zero() == (y[i, 0]))
                            x[i, 0] = Rational.Random(); //so we provide a nonzero (probably) value for the free variable.
                        else
                            x[i, 0] = (x[i, 0]+ y[i, 0]);
                    }
                    else
                    {
                        x[i, 0] = (x[i, 0]- (LU[i, j]* x[j, 0]));
                    }
                }
                if (!(Rational.Zero() == LU[i, i]))
                    x[i, 0] = (x[i, 0]/ LU[i, i]);
            }
            //			Console.Write ("x = ");
            //			Console.WriteLine (new Matrix<T,C> (x));
            //
            return new Matrix(x);
        }

        // ReSharper disable once InconsistentNaming
        public Matrix Solve(Matrix B)
        {
            if (B.Rows() != _m)
                throw new ArgumentException("rows don't match");
            if (!IsNonSingular)
                throw new SystemException("singular.");
            int nx = B.Columns();
            Rational[,] x = B.GetMatrix(_piv, 0, nx - 1).ToArray();

            // Solve L*Y = B(piv,:)
            for (int k = 0; k < _n; k++)
                for (int i = k + 1; i < _n; i++)
                    for (int j = 0; j < nx; j++)
                        x[i, j] = (x[i, j]- (x[k, j]* LU[i, k]));

            // Solve U*X = Y;
            for (int k = _n - 1; k >= 0; k--)
            {
                for (int j = 0; j < nx; j++)
                    x[k, j] = (x[k, j]/ LU[k, k]);
                for (int i = 0; i < k; i++)
                    for (int j = 0; j < nx; j++)
                        x[i, j] = (x[i, j]- (x[k, j]* LU[i, k]));

            }




            return new Matrix(x);
        }
    }
}




















