﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANT
{
    public class Utils
    {
        public static Rational Resultant(Rational[] a, Rational[] b)
        {
            Array.Reverse(a);
            Array.Reverse(b);
            int length = a.Length + b.Length-2;
            Rational[,] rmat = new Rational[length, length];
            for (int i = 0; i < length; i++)
            {

                for (int j = 0; j < length; j++)
                {
                    rmat[i, j] = Rational.Zero() ;
                }

            }
            int count = 0;
            bool donea = false;
            for (int i = 0; i < length; i++)
            {
                if (count >= b.Length-1 && !donea)
                {
                    donea = true;
                    count = 0;
                }
                for (int j = 0; j < length; j++)
                {
                    
                    if (!donea)
                    {
                        if (j+count < length && j < a.Length)
                            rmat[i, j + count] = a[j];

                    }else
                    {
                        if (j + count < length&& j < b.Length)
                            rmat[i, j + count] = b[j];
                    }
                }
                count++;
            }
            
            Matrix mat = new Matrix(rmat);
            Console.WriteLine(mat);
            //mat = Matrix<Rational, LinearAlgebra.MathProviders.RationalMathProvider>.RowReducedForm(mat);

            return mat.Determinant();
        }

        public static Rational PolynomialDiscriminant(Rational[] p)
        {
            Rational res = 0;

            Rational[] pprime = new Rational[p.Length - 1];
            for (int i = 1; i < p.Length; i++ )
            {
                pprime[i - 1] = i * p[i];
            }

            Rational resultant = Resultant(p, pprime);

            //subtract 1 from p.length to get degree of poly
            int m = ((p.Length-1) * (p.Length - 2)) / 2;
            Rational sgn = -1;
            sgn = sgn.Pow(m);
            //p is reversed at this point
            Rational inva = p[0];

            return sgn * inva * resultant;
        }
        public static Rational PolynomialContent(Rational[] p)
        {
            Rational result = p[0];
            for (int i = 1; i < p.Length; i++)
            {
                result = Rational.GCD(result, p[i]);
            }
            return result;
        }
    }
}
