﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace ANT
{
    public class NumberField
    {
        public Polynomial MinimalPolynomial;
        public Complex Theta;
        public int Dimension;
        public Polynomial ReductionPolynomial;


        public NumberField(Polynomial minpoly, Complex theta)
        {
            this.MinimalPolynomial = minpoly;
            this.Theta = theta;
            this.Dimension = minpoly.Degree;

            var temp = minpoly - new Polynomial(minpoly[this.Dimension],this.Dimension);
            this.ReductionPolynomial = (new Rational(-1)) * temp;
        }

        /// <summary>
        /// Creates a Algebraic number from the given coefficients which are considered as coefficients of the power basis representation
        /// </summary>
        /// <param name="coefs"></param>
        /// <returns></returns>
        public AlgebraicNumber CreateFieldElement(Rational[] coefs)
        {
            return new AlgebraicNumber(coefs, this);
        }
        /// <summary>
        /// Creates a new Number field Q(Gamma) = Q(alpha, beta):Q(alpha)
        /// and finds a primitive element over that field
        /// </summary>
        /// <param name="minpoly"></param>
        /// <returns></returns>
        public NumberField Adjoin(Polynomial minpoly)
        {
            throw new NotImplementedException();
        }


    }
}
