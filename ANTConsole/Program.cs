﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ANT;

using System.Numerics;
namespace ANTConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Rational[] qi = new Rational[2]{2,1};
            // Rational[] minp = new Rational[3] { 1, 1, 1 };
             //Complex theta = new Complex(-0.5, 0.866025);//-0.5 + 0.866025 I
             //int dim = 2;
             //AlgebraicNumber q = new AlgebraicNumber(qi, dim, theta, minp);

             //Console.WriteLine(q);
             //Console.WriteLine(q.eval());
             //Console.WriteLine(q.Norm());
             //Console.WriteLine(q + q);
             //Console.WriteLine(q - q);
            //Console.WriteLine(q * q);
            //Console.WriteLine((q * q).eval());

            //Rational[] p = new Rational[4] { 5, 2, 0,1 };
            //Rational[] q = new Rational[3] { 2, 0, 3 };

            //Console.WriteLine(ANT.Utils.resultant(p,q));
            //Console.WriteLine(Utils.PolynomialDiscriminant(p));

            //Rational[] polyc = new Rational[2] { -1,1};
            Rational[] polyc2 = new Rational[] { 4,4,1};
            //Polynomial poly = new Polynomial(polyc);
            Polynomial poly2 = new Polynomial(polyc2);
            //Console.WriteLine(poly);
            //Console.WriteLine(poly2);
            //var _2p = poly * poly2;
            //Console.WriteLine(_2p);
            //Console.WriteLine(poly2.Eval(2));
            //Polynomial[] qr = Polynomial.EuclideanDivision(poly2, poly);
            //Console.WriteLine(qr[0]);
            //Console.WriteLine(qr[1]);
            //Console.WriteLine(new Polynomial(new Rational[0] { }));
            //Console.WriteLine(Polynomial.GCD(poly2*poly, poly*poly));
            //Console.WriteLine(poly2.CompanionMatrix());
            //Console.WriteLine(new Polynomial(1,23));

            Console.WriteLine(poly2);
            Console.WriteLine(poly2.Reduce(new Polynomial(new Rational[] { -1, -1 }), 2));

            Console.WriteLine(Utils.Resultant(new Rational[] { 1, 23 }, new Rational[] { 1, 1, 1, 1, 1 }));
            Console.ReadLine();
        }
    }
}
